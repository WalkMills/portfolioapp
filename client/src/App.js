import { Layout } from "./components/Layout";
import { Login } from "./components/user/Login"
import { RequireAuth } from "./app/RequireAuth";
import { PortfolioView } from "./components/PortfolioView";
import { Routes, Route } from 'react-router-dom'
import { SignUp } from "./components/user/SignUp";
import { AddPortfolio } from "./components/AddPortfolio";
import { StockView } from "./components/StockView";
import { AddStockForm } from "./components/addStockForm";


function App() {
  return (
    <Routes >
      <Route path="/" element={<Layout/>}>
        {/* public routes */}
        <Route path="login" element={<Login/>} />
        <Route path="signup" element={<SignUp />} />

        {/* protected routes */}
        <Route element={<RequireAuth />}>
          <Route path="portfolioview" element={<PortfolioView/>} />
          <Route path="addportfolio" element={<AddPortfolio />} />
          <Route path="stockview/:portfolioId" element={<StockView />} />
          <Route path="addstock/:portfolioId" element={<AddStockForm />}/>
        </Route>

      </Route>
    </Routes>
  )
}

export default App;
