import { createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    username: null,
    token: null,
    error: null
  },
  reducers: {
    setCredentials: (state, action) => {
      try {

        const { token, username } = action.payload;
        state.username = username;
        state.token = token;
        console.log("inside reducer", state.token)
        state.error = null
      } catch (e) {
        state.error = e.message
      }
    },
    logOut: (state, action) => {
      try {
        state.username = null
        state.token = null
        state.error = null

        console.log("inside logOut", state.token)
      } catch (e) {
        state.error = e.message
      }
    }
  }
})

export const { setCredentials, logOut } = authSlice.actions

export default authSlice.reducer

export const selectCurrentUser = (state) => state.auth.username
export const selectCurrentToken = (state) => state.auth.token
export const selectError = (state) => state.auth.error
