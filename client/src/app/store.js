import { configureStore } from '@reduxjs/toolkit'
import { setupListeners} from '@reduxjs/toolkit/query'
import { portfolioApi } from './apiSlice'
import  authReducer from './authSlice'





export const store = configureStore({
    reducer: {
        [portfolioApi.reducerPath]: portfolioApi.reducer,
        auth: authReducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(portfolioApi.middleware),
    devTools: true
})

// setupListeners(store.dispatch)