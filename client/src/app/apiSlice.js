import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'



export const portfolioApi = createApi({
    baseQuery: fetchBaseQuery({
        baseUrl: "http://localhost:8081",
        credentials: 'include',
        prepareHeaders: (headers, { getState }) => {
            const token = getState().auth.token
            if (token) {
                console.log("there is a token")
                headers.set("authorization", `Bearer ${token}`)
            } else {console.log('no token')}
            return headers
        }
    }),
    endpoints: builder => ({
        login: builder.mutation({
            query: (formData) => ({
                    url: '/signin',
                    body: {
                        "username": formData.username,
                        "password": formData.password
                    },
                    method: 'POST',
                    
            })
        }),
        signUp: builder.mutation({
            query: (formData) => ({
                url: '/user',
                body: {
                    "username": formData.username,
                    "password": formData.password
                },
                method: 'POST',                
            })
        }),
        getPortfolios: builder.query({
            query: () => ({
                url: '/api/portfolio',
            }),
            providesTags: ['Portfolio'],
            transformResponse: (response) => response.data
        }),
        createPortfolio: builder.mutation({
            query: (formData) => ({
                url: '/api/portfolio',
                method: 'POST',
                body: {
                    "name": formData.name
                }
            }),
            invalidatesTags: ['Portfolio']
        }),
        getPortfolioDetails: builder.query({
            query: (id) => ({
                url: `/api/portfolio/${id}`,
                credentials: "include"
            }),
            transformResponse: (response) => response.data.stocks,
            providesTags: ["Portfolio"]
        }),
        getStocks: builder.query({
            query: (id) => ({
                url: `/api/stocks/${id}`
            }),
            providesTags: ['Stocks'],
            transformResponse: (response) => response.data
        }),
        createStocks: builder.mutation({
            query: (data) => ({
                url: `/api/stocks/${data.id}`,
                method: "POST",
                body: data.body
            }),
            invalidatesTags: ["Portfolio"]
        })
        
    })
})


export const {
    useLoginMutation,
    useSignUpMutation,
    useGetPortfoliosQuery,
    useCreatePortfolioMutation,
    useGetPortfolioDetailsQuery,
    useGetStocksQuery,
    useCreateStocksMutation
} = portfolioApi