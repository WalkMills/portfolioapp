import { useState } from "react";
import { useCreatePortfolioMutation } from "../app/apiSlice";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectCurrentToken } from "../app/authSlice";


export const AddPortfolio = () => {
    const [formData, setFormData] = useState({
        "name": ''
    })
    const [portfolio] = useCreatePortfolioMutation()
    const navigate = useNavigate()

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }
    const t = useSelector(selectCurrentToken)


    const handleSubmit = (event) => {
        event.preventDefault()

        portfolio(formData)
        setFormData({"name": ''})
        navigate('/portfolioview')
    }

    return (
        <div>
        <div>
            <h2>Add Portfolio</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="name">Portfolio Name</label>
                    <input required type="text" id="name" name="name" onChange={handleChange}/>
                </div>
                <button>Submit</button>
            </form>
        </div>
    </div>
    )
}