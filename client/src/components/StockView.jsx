import { useGetPortfolioDetailsQuery, useGetStocksQuery } from "../app/apiSlice";
import { useParams, NavLink } from "react-router-dom";
import { useSelector } from 'react-redux';
import { selectCurrentToken } from "../app/authSlice";
import { Nav } from "./Nav";


export const StockView = () => {
    const id = useParams()
    console.log(id.portfolioId)

    const {data, isLoading} = useGetPortfolioDetailsQuery(id.portfolioId)

    const token = useSelector(selectCurrentToken)
    console.log(token)

    console.log("data", data)
    
    if (isLoading) return <div>Loading...</div>

    return (
        <div>
            <div>
                <Nav />
                <h2>Portfolio</h2>
                <table>
                    <thead>
                        <tr>
                            <th>Ticker</th>
                            <th>Full Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            {data.map(stock => {
                                return (
                                    <tr>
                                        <td>{stock.ticker}</td>
                                        <td>{stock.fullName}</td>
                                    </tr>
                                )
                            })}
                    </tbody>
                </table>
                <NavLink to={`/addstock/${id.portfolioId}`}><button>add stock</button></NavLink>
            </div>
        </div>
    )
}