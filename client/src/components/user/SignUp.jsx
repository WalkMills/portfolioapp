import { useState } from 'react'
import { useSignUpMutation } from '../../app/apiSlice'
import { useDispatch } from 'react-redux'
import { setCredentials } from '../../app/authSlice'
import { useNavigate } from 'react-router-dom'


export const SignUp = () => {
    const [formData, setFormData] = useState({
        "username": '',
        "password": ''
    })

    const [signUp] = useSignUpMutation()
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        try {
            const userData = await signUp(formData).unwrap()
            const user = formData.username
            const token = userData.token

            dispatch(setCredentials({"username": user, "token": token}))

            setFormData({"username": '', "password": ''})

            navigate('/portfolioview')
        } catch (err) {
            if (!err?.originalStatus) {
                console.log('no server response')
            } else if (err.respone?.status === 400) {
                console.log('missing username or password')
            } else if (err.response?.status == 401) {
                console.log('unauthorized')
            } else {
                console.log('login failed')
            }
        }
    }

    return (
        <div>
            <div>
                <h2>Sign Up</h2>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="username">Username</label>
                        <input required type="text" id="username" name="username" onChange={handleChange}/>
                    </div>
                    <div>
                        <label htmlFor="password">Password</label>
                        <input required type="password" id="password" name="password" onChange={handleChange}/>
                    </div>
                    <button>Submit</button>
                </form>
            </div>
        </div>
    )
}