import { useGetPortfoliosQuery } from "../app/apiSlice"
import { useSelector } from 'react-redux';
import { selectCurrentToken } from "../app/authSlice";
import { Nav } from "./Nav";
import { useNavigate, NavLink } from "react-router-dom";


export const PortfolioView = () => {


    const {data, isLoading} = useGetPortfoliosQuery()
    const token = useSelector(selectCurrentToken)
    const navigate = useNavigate()

    console.log(data)
    if (!token) {
        return <div>Loading...</div>
    } else if (!isLoading) {
        return (
            <>
                <Nav />
                <div>
                    <table>
                        <thead>
                            <td>
                                <tr>Portfolio Name</tr>
                            </td>
                        </thead>
                        <tbody>
                            <td>
                            {data.map((p) => {
                                 return (
                                    <>
                                        <tr>
                                            <td>{p.name}</td>
                                            <td><NavLink to={`/stockview/${p.id}`}><button>view</button></NavLink></td>
                                        </tr>
                                    </>
                                )
                            })}
                            </td>
                        </tbody>
                    </table>
                </div>
                <NavLink to={'/addportfolio'}><button>Add Portfolio</button></NavLink>
            </>
        )
    }


}