import { useParams } from "react-router-dom";
import { useState } from "react";
import { useCreateStocksMutation } from "../app/apiSlice";
import { useNavigate } from "react-router-dom";

export const AddStockForm = () => {
    const id = useParams()
    console.log(id)
    const [stock] = useCreateStocksMutation()
    const [formData, setFormData] = useState({
        'ticker': '',
        'fullName': '',
    })
    const navigate = useNavigate()
    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        const data = {
            body: formData,
            id: id.portfolioId
        }
        console.log(formData)
        stock(data)
        navigate(`/stockview/${data.id}`)
    }


    return (
        <div>
            <div>
                <h2>Add Stock Entry</h2>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="ticker">Ticker</label>
                        <input required onChange={handleChange} name="ticker" type="text"/>
                    </div>
                    <div>
                        <label htmlFor="fullName">Full Name</label>
                        <input required onChange={handleChange} name="fullName" type="text"/>
                    </div>
                    <button>submit</button>
                </form>
            </div>
        </div>
    )

}