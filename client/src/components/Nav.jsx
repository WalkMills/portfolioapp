import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux"
import { selectCurrentToken, logOut } from "../app/authSlice";
import "./Nav.css"
import { NavLink } from "react-router-dom";

export const Nav = () => {
    const navigate = useNavigate()
    const token = useSelector(selectCurrentToken)


    const handleLogout = () => {
        logOut()
    }

    return (
        <nav>
            <div>
                <div>
                    <ul className="nav-style">
                        { token && <li className="list-style" name="portfolioview"><NavLink to={'/portfolioview'}>portfolios</NavLink></li>}
                        { token && <li className="list-style" ><NavLink to={'/login'} onClick={handleLogout}>logout</NavLink></li>}
                    </ul>
                </div>
            </div>
        </nav>
    )

}