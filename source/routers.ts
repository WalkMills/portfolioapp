import { Router } from "express";
import { createStock, deleteStock, getOneStock, getStocks, updateStock } from "./handlers/stocks";
import { createPortfolio, deletePortfolio, getOnePortfolio, getPortfolio, updatePortfolio } from './handlers/portfolio'


export const router = Router()



router.get('/stocks/:portfolioId', getStocks)
router.get('/stocks/:id', getOneStock)
router.post('/stocks/:id', createStock)
router.put('stocks/:id', updateStock)
router.delete('stocks/:id', deleteStock)


router.post('/portfolio', createPortfolio)

router.get('/portfolio/:id', getOnePortfolio)

router.get('/portfolio', getPortfolio)

router.put('/portfolio/:id', updatePortfolio)

router.delete('/portfolio/:id', deletePortfolio)
