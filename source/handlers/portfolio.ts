import { prisma } from '../db'


export const getPortfolio = async (req, res) => {
    console.log(req.user)
    try {
        const user = await prisma.user.findUnique({
            where: {
                id: req.user.id
            },
            include: {
                portfolio: true
            }
        })
    
        res.json({data: user?.portfolio})
    } catch (e) {
        console.log(e)
    }
}


export const getOnePortfolio = async (req, res) => {

    console.log(req.user.id)

    const portfolio = await prisma.portfolio.findUnique({
        where: {
            id: req.params.id,
            userId: req.user.id
        },
        include: {
            stocks: true
        }
    })


    res.json({data: portfolio})

}

export const createPortfolio = async (req, res, next) => {
    try {
        const portfolio = await prisma.portfolio.create({
            data: {
                name: req.body.name,
                userId: req.user.id
            }
        })

        res.json({data: portfolio})
    } catch (e) {
        next(e)
    }
}


export const updatePortfolio = async (req, res) => {
    const updatedPortfolio = await prisma.portfolio.update({
        where: {
            id: req.params.id,
            userId: req.user.id
        },
        data: {
            name: req.body.name
        }
    })

    res.json({data: updatedPortfolio})
}


export const deletePortfolio = async (req, res) => {
    const deleted = await prisma.portfolio.delete({
        where: {
            id: req.params.id,
            userId: req.user.id
        }
    })
    
    res.json({data: deleted})
}