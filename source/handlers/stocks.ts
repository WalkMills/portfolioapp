import { prisma } from "../db";


export const getStocks = async (req, res) => {
    const portfolio = await prisma.portfolio.findUnique({
        where: {
            id: req.params.portfolioId,
            userId: req.user.id
        },
        include: {
            stocks: true
        }
    })


    res.json({data: portfolio.stocks})
}


export const getOneStock = async (req, res) => {
    const stock = await prisma.stock.findUnique({
        where: {
            id: req.params.id
        }
    })

    const portfolio = await prisma.portfolio.findUnique({
        where: {
            id: stock.portfolioId,
            userId: req.user.id
        }
    })

    if (!portfolio) {
        return res.json({message: "this portfolio does not belong to you"})
    } else {
        res.json({data: stock})
    }
}


export const createStock = async (req, res) => {
    const portfolio = await prisma.portfolio.findUnique({
        where: {
            id: req.params.id,
            userId: req.user.id
        }
    })

    if (!portfolio) {return res.json({message: "portfolio does not belong to you"})}

    req.body.portfolioId = portfolio.id

    const stock = await prisma.stock.create({
        data: req.body
    })

    res.json({data: stock})
}


export const updateStock = async (req, res) => {
    const stock = await prisma.stock.findUnique({
        where: {
            id: req.params.id
        }
    })

    const portfolio = await prisma.portfolio.findUnique({
        where: {
            id: stock.portfolioId,
            userId: req.user.id
        }
    })

    if (!portfolio) {return res.json({message: "portfolio does not belong to you"})}

    const updatedStock = await prisma.stock.update({
        where: {
            id: req.params.id
        },
        data: req.body
    })

    res.json({data: updatedStock})
}


export const deleteStock = async (req, res) => {
    const stock = await prisma.stock.findUnique({
        where: {
            id: req.params.id
        }
    })

    const portfolio = await prisma.portfolio.findUnique({
        where: {
            id: stock.portfolioId,
            userId: req.user.id
        }
    })

    if (!portfolio) {return res.json({message: "portfolio does not belong to you"})}

    const deleted = await prisma.stock.delete({
        where: {
            id: req.params.id
        }
    })

    res.json({data: deleted})
}