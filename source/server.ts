import express from 'express'
import { router } from './routers'
import { protect } from './modules/auth'
import { createUser, signIn } from './handlers/user'
import morgan from 'morgan'
import cors from 'cors'


export const app = express()



app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: true}))


app.use(cors({
    origin: "http://localhost:3000",
    credentials: true
}))
app.use('/api', protect, router)
app.post('/user', createUser)
app.post('/signin', signIn)
